//
//  ViewController.swift
//  Tic Tac Toe
//
//  Created by Samar Singla on 02/02/15.
//  Copyright (c) 2015 Gupta&sons. All rights reserved.
//

import UIKit

// Global Variables used to get Players Name from Differnt ViewController
var playerOne = "PlayerOne"
var playerTwo = "PlayerTwo"
// Global Array used to get all Players Logs 
var getRecord:[String] = []

class ViewController: UIViewController {
    // ImageView
    @IBOutlet weak var tictactoeBoard: UIImageView!
    // Labels
    @IBOutlet weak var scoreBoardview: UIView!
    @IBOutlet weak var firstPlayer: UILabel!
    @IBOutlet weak var secondPlayer: UILabel!
    @IBOutlet weak var wonByfirstplayer: UILabel!
    @IBOutlet weak var wonBysecondplayer: UILabel!
    @IBOutlet weak var numberOfties: UILabel!
    @IBOutlet weak var playerTurns: UILabel!
    
    let winningCombination = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]]// constant Array used to get all WinningCombination
    var getStatus = [ 0, 0, 0, 0, 0, 0, 0, 0, 0 ]   // Array used to get the Button status #0 = empty , 1 = cross , 2 = nought
    var winner = 0  // variable used to sotre won case values
    var winMessage = "" // variable used to get winner name
    var numberofbuttonTap = 0 // variable used to count Button Tap
    var numberofgamePlay = 0  // variable used to count Game Play
    var player1Wons = 0  // variable used to count player one Won
    var player2Wons = 0  // variable used to count Player Two won
    var tiesCounter = 0  // variable used to count Ties
    // Button
    @IBAction func buttonPressed(sender: AnyObject) {
        if getStatus[sender.tag] == 0  && winner == 0{ // Condition for check the Buttton Status with the help of Button Tags i.e 0 to 9
            numberofbuttonTap++
            var image = UIImage()
            
            if goNumber%2 == 0 {  // condition for check the Player1 Option i.e either Cross or Nought
                image = UIImage(named: "nought.png")!
                getStatus[sender.tag] = 2
            } else {
                image = UIImage(named: "cross.png")!
                getStatus[sender.tag] = 1
            }
            for combination in winningCombination { // Condition for Check the all 9 WinningCombination
                if getStatus[combination[0]] == getStatus[combination[1]] && getStatus[combination[1]] == getStatus[combination[2]] && getStatus[combination[0]] != 0{
                    winner = getStatus[combination[0]]
                }
            }
            if winner != 0 { // Condition for check if someone has won the Game
                if winner == playerState{   // if Cross has Won
                    winMessage = "\(playerOne.uppercaseString) Has Won 👍 "
                    ++player1Wons
                    wonByfirstplayer.text = "\(player1Wons)" // Displaying Player1 Wons in ScoreBoard
                }else {    // if Nought has Won
                    winMessage = "\(playerTwo.uppercaseString) Has Won 👍"
                    ++player2Wons
                    wonBysecondplayer.text = "\(player2Wons)" // Displaying Player2 Wons in ScoreBoard
                }
                // AlertView used to Show the Result and Giving option to Playagian by Reseting all Game
                var alert1 = UIAlertController(title: "RESULT", message: winMessage, preferredStyle: UIAlertControllerStyle.Alert)
                alert1.addAction(UIAlertAction(title: "PlayAgain", style: UIAlertActionStyle.Default){ (action) in
                    self.playAgian()
                    })
                // AlertView Action used to show the Players ScoreBoard
                alert1.addAction(UIAlertAction(title: "ScoreBoard", style: UIAlertActionStyle.Default){ (action) in
                    self.scoreBoard()
                    })
                self.presentViewController(alert1, animated: true, completion: nil)
                
            }else if winner == 0 && numberofbuttonTap == 9 {  // Condition for check if there is a Tie
                ++tiesCounter
                numberOfties.text = "\(tiesCounter)"
                // AlertView used to Show the Result and Giving option to Playagian by Reseting all Game
                var alert1 = UIAlertController(title: "RESULT", message: "OOPS! Its a Tie 😯", preferredStyle: UIAlertControllerStyle.Alert)
                alert1.addAction(UIAlertAction(title: "PlayAgain", style: UIAlertActionStyle.Default){ (action) in
                    self.playAgian()
                    })
                // AlertView Action used to show the Players ScoreBoard
                alert1.addAction(UIAlertAction(title: "ScoreBoard", style: UIAlertActionStyle.Default){ (action) in
                    self.scoreBoard()
                    })
                self.presentViewController(alert1, animated: true, completion: nil)
            }
            goNumber++
            sender.setImage(image, forState: .Normal)
        }                                                                            // Button used to Tap Player option i.e either Cross or Nought
    } //Button Clossed

    
    @IBAction func cancelPressed(sender: AnyObject) {
        numberofgamePlay = 0                                 // Cancel Button used to Quit the Game and Switch to Initial ViewController
    }
    @IBAction func backButton(sender: AnyObject) {
        scoreBoardview.alpha = 0
        self.tictactoeBoard.alpha = 1
        playerTurns.alpha = 1                                            // Back Button used to Switch from ScoreBoard to TicTacToe Board
    }
    @IBAction func logSubmit(sender: AnyObject) {
        self.saveRecord()
    }
    
    func saveRecord(){ // Function for saving Records in Persistent Storge manner
        let record =  "\(playerOne) vs \(playerTwo)             \(player1Wons)-\(player2Wons)            \(tiesCounter)"
        getRecord.append(record)
        var fixedRecord = getRecord
        NSUserDefaults.standardUserDefaults().setObject(fixedRecord, forKey:"getRecord")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func playAgian() { // its a PlayAgain Function which is call in AlertView
        self.numberofgamePlay++
        self.winner = 0
        self.numberofbuttonTap = 0
        self.getStatus = [ 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
        var button : UIButton
        for index in 0..<9 {
            button = self.view.viewWithTag(index) as UIButton
            button.setImage(nil, forState: .Normal)
        }
        if self.numberofgamePlay%2 == 0 {
            self.playerTurns.text = " Now \(playerOne.uppercaseString) Has a Turn First"
        } else if self.numberofgamePlay%2 == 1 {
            self.playerTurns.text = "Now \(playerTwo.uppercaseString) Has a Turn First"
        }
    }
    
    func scoreBoard() { // its a ScoreBoard Function which is call in AlertView
        self.tictactoeBoard.alpha = 0
        self.scoreBoardview.alpha = 1
        self.playerTurns.alpha = 0
        self.firstPlayer.text = playerOne.uppercaseString
        self.secondPlayer.text = playerTwo.uppercaseString
        self.winner = 0
        self.numberofbuttonTap = 0
        self.getStatus = [ 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
        var button : UIButton
        for index in 0..<9 {
            button = self.view.viewWithTag(index) as UIButton
            button.setImage(nil, forState: .Normal)
        }
    }
    
    override func supportedInterfaceOrientations() -> Int {
        return UIInterfaceOrientationMask.Portrait.rawValue.hashValue
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        scoreBoardview.alpha = 0
        tictactoeBoard.alpha = 1
        playerTurns.alpha = 1

        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

