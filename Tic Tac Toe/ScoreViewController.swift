//
//  ScoreViewController.swift
//  Tic Tac Toe
//
//  Created by Samar Singla on 10/02/15.
//  Copyright (c) 2015 Gupta&sons. All rights reserved.
//

import UIKit

class ScoreViewController: UIViewController, UITableViewDelegate {
    
      @IBOutlet var scoreView: UITableView!
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getRecord.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // it takes a Cell value in Table
        let cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        cell.textLabel?.text = "\(indexPath.row + 1):            \(getRecord[indexPath.row])"
        return cell
    }                                                                                 // Cell Displaying Function
    
    override  func viewWillAppear(animated: Bool) {
        if var storedtoDoItem: AnyObject = NSUserDefaults.standardUserDefaults().objectForKey("getRecord") {
            getRecord = []
            for var i = 0; i < storedtoDoItem.count ; ++i {
                getRecord.append(storedtoDoItem[i] as NSString)
            }
        }                                                                       // Cell Adding Function
        scoreView.reloadData() // it Reload Data in Table
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath){
        if editingStyle == UITableViewCellEditingStyle.Delete {
            // it remove the from its indexpath
            getRecord.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
            // For updating Presistent Storage
            var fixedRecord = getRecord
            NSUserDefaults.standardUserDefaults().setObject(fixedRecord, forKey:"getRecord")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }                                                                      // Cell Removing function
    
    override func supportedInterfaceOrientations() -> Int {
        return UIInterfaceOrientationMask.Portrait.rawValue.hashValue
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //println("\(playerCount) || \(winCount) || \(drawCount)")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
