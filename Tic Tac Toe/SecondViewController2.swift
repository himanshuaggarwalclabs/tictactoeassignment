//
//  ViewController2.swift
//  Tic Tac Toe
//
//  Created by Samar Singla on 04/02/15.
//  Copyright (c) 2015 Gupta&sons. All rights reserved.
//

import UIKit
var goNumber = 1  // Global Variable to get Button Status
var playerState = 2 // Global Variable to Track Player State

class ViewController2: UIViewController {
     // TextFields
    @IBOutlet weak var player1: UITextField! //TextField used to get PlayerOne Name
    @IBOutlet weak var player2:UITextField!  //TextField used to get PlayerTwo Name
    // Labels
    @IBOutlet weak var playerController: UILabel! // used to display Player1 Controller i.e. either Cross or Nought
    @IBOutlet weak var defaultController: UILabel!// used to display Default Controller i.e. either Cross or Nought
    
    @IBAction func playButton(sender: AnyObject) {
        if player1.text == "" {
            playerOne = "Player1"
        }else {
            playerOne = player1.text
        }
        if player2.text == "" {
            playerTwo = "Player2"
        } else {
            playerTwo = player2.text
        }                                   // Button Used to Start the Game After getting Players Name by Switching it to another ViewController
    }
    
    @IBAction func crossButton(sender: AnyObject) {
        goNumber = 1
        playerController.text = "Player1"
        defaultController.text = "Player2"
        playerState = 1                                      // Button used to select Player Icon i.e Cross
    }

    @IBAction func NoughtButton(sender: AnyObject) {
        goNumber = 2
        playerController.text = "Player2"
        defaultController.text = "Player1"
        playerState = 2                                    // Button used to select Player Icon i.e Cross
    }
    override  func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.view.endEditing(true)
                                                                            // KeyBoard Manging Function
    }
    override func supportedInterfaceOrientations() -> Int {
        return UIInterfaceOrientationMask.Portrait.rawValue.hashValue
    }
    func textField(player1: UITextField!, shouldChangeCharactersInRange range: NSRange, replacementString string: String!) -> Bool {
        
        let newLength = countElements(player1.text!) + countElements(string!) - range.length
        //let newLength = countElements(player2.text!) + countElements(string!) - range.length

        return newLength <= 7 //Bool
        
    }
    
}
